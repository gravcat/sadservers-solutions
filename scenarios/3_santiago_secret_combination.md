```bash
cd ~
total_special_string_count=0
for file in $(ls *.txt); do 
    special_string_in_file_count=$(grep 'Alice' $file | wc -l)
    if [ $special_string_in_file_count == 1 ]; then
        echo $file
    fi
    total_special_string_count=$(($total_special_string_count+$special_string_in_file_count))
done
1342-0.txt

vi 1342-0.txt
 ?Alice
 156

echo -n $total_special_string_count > ~/solution; echo 156 >> ~/solution