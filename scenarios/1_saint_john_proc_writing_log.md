```bash
lsof /var/log/bad.log
```

```
COMMAND   PID   USER   FD   TYPE DEVICE SIZE/OFF  NODE NAME
badlog.py        622    ubuntu   3w    REG  259,1      8677 67701 /var/log/bad.log
```

```
kill 622
```